# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model
from django.utils.text import capfirst
from django.contrib.auth.models import check_password
from .models import Courier
import warnings

def authenticate(cellphone=None, password=None):
        cellphone_valid = Courier.objects.is_cellphone(cellphone)
        password_valid = check_password(password, Courier.objects.get(cellphone=cellphone).password)

        if cellphone_valid and password_valid:
            try:
                courier = Courier.objects.get(cellphone=cellphone)
                return courier
            except Courier.DoesNotExist:
                return None

        return None

class AuthenticationForm(forms.ModelForm):
    class Meta:
        model = Courier
        fields = ['cellphone', 'password']

    def clean(self):
        cellphone = self.cleaned_data.get('cellphone')
        password = self.cleaned_data.get('password')

        if cellphone and password:
            self.user_cache = authenticate(cellphone=cellphone,
                                           password=password)
            if self.user_cache is None:
                raise forms.ValidationError(
                    self.error_messages['invalid_login'],
                    code='invalid_login',
                    params={'cellphone': self.cellphone_field.verbose_name},
                )
            elif not self.user_cache.is_active:
                raise forms.ValidationError(
                    self.error_messages['inactive'],
                    code='inactive',
                )
        return self.cleaned_data

    def check_for_test_cookie(self):
        warnings.warn("check_for_test_cookie is deprecated; ensure your login "
                "view is CSRF-protected.", DeprecationWarning)

    def get_user_id(self):
        if self.user_cache:
            return self.user_cache.id
        return None

    def get_user(self):
        return self.user_cache

