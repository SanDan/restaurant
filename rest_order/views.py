# -*- coding: utf-8 -*-
from django.contrib.auth.models import check_password
from rest_personal.views import login
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.views.generic.base import View
from django.contrib.sessions.models import Session
import json
from django.core.serializers.json import DjangoJSONEncoder

from rest_personal.models import Courier
from .models import Order

########################################################################################################################


def authenticate(cellphone=None, password=None):
        cellphone_valid = Courier.objects.is_cellphone(cellphone)
        password_valid = check_password(password, Courier.objects.get(cellphone=cellphone).password)

        if cellphone_valid and password_valid:
            try:
                courier = Courier.objects.get(cellphone=cellphone)
                return courier
            except Courier.DoesNotExist:
                return None

        return None


class OrdersForCourier(View):

    def post(self, request, *args, **kwargs):
        cellphone = request.POST["cellphone"]
        password = request.POST["password"]

        user = authenticate(cellphone, password)
        if user:
            user.backend = 'rest_personal.backends.RestBackend'
            login(request, user)
            return HttpResponse(json.dumps({'SID': request.session.session_key}), status=200)
        return HttpResponse(status=401)

    def get(self, request, *args, **kwargs):
        SID = request.GET['SID']
        s = Session.objects.get(pk=SID)
        courier_id = s.get_decoded()['_auth_user_id']

        courier = Courier.objects.get(id=courier_id)

        orders = list(Order.objects.filter(courier=courier).values('date_create', 'address', 'status'))
        for index, order in enumerate(orders):
            o = orders.pop(index)
            o = json.dumps(o, cls=DjangoJSONEncoder)
            orders.append(o)

        orders = json.dumps(orders, cls=DjangoJSONEncoder)
        return HttpResponse(json.dumps({"orders": orders}))

    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(OrdersForCourier, self).dispatch(request, *args, **kwargs)