# -*- coding: utf-8 -*-
from .models import Courier
from django.contrib.auth.models import check_password

class RestBackend(object):

    def authenticate(self, cellphone=None, password=None):
        cellphone_valid = Courier.objects.is_cellphone(cellphone)
        password_valid = check_password(password, Courier.objects.get(cellphone=cellphone).password)

        print cellphone_valid
        print password_valid
        if cellphone_valid and password_valid:
            try:
                courier = Courier.objects.get(cellphone=cellphone)
                return courier
            except Courier.DoesNotExist:
                return None

        return None

    def get_user(self, user_id):
        try:
            return Courier.objects.get(pk=user_id)
        except Courier.DoesNotExist:
            return None
