# -*- coding: utf-8 -*-
from django.contrib import admin
from django import forms

from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.forms import ReadOnlyPasswordHashField

from .models import RestUser, Courier

########################################################################################################################

class RestUserCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'Повторите пароль', widget=forms.PasswordInput)

    class Meta:
        model = RestUser
        fields = ('email', 'name')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Проверьте пароль")
        return password2

    def save(self, commit=True):
        user = super(RestUserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class RestUserChangeForm(forms.ModelForm):

    password = ReadOnlyPasswordHashField()

    class Meta:
        model = RestUser
        fields = ('email', 'password', 'name', 'is_active', 'is_admin', )

    def clean_password(self):
        return self.initial["password"]


class RestUserAdmin(UserAdmin):
    form = RestUserChangeForm
    add_form = RestUserCreationForm

    list_display = ('email', 'password', 'name', 'is_admin', )
    list_filter = ('is_admin', )

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (u'Персональные данные', {'fields': ('name', )}),
        (u'Права доступа', {'fields': ('is_admin', 'is_active', )}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide', ),
            'fields': ('email', 'name', 'password1', 'password2')
        }),
    )

    search_fields = ('email', )
    ordering = ('email', )
    filter_horizontal = ()

admin.site.register(RestUser, RestUserAdmin)

admin.site.unregister(Group)


########################################################################################################################

class CourierCreationForm(forms.ModelForm):
    password1 = forms.CharField(label=u'Пароль', widget=forms.PasswordInput)
    password2 = forms.CharField(label=u'Повторите пароль', widget=forms.PasswordInput)

    class Meta:
        model = Courier
        fields = ('cellphone', 'name')

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Проверьте пароль")
        return password2

    def save(self, commit=True):
        user = super(CourierCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user

class CourierChangeForm(forms.ModelForm):

    password = ReadOnlyPasswordHashField()

    class Meta:
        model = Courier
        fields = ('cellphone', 'password', 'name', 'is_active', 'is_staff', 'is_work', )

    def clean_password(self):
        return self.initial["password"]


class CourierAdmin(UserAdmin):
    form = CourierChangeForm
    add_form = CourierCreationForm

    list_display = ('cellphone', 'name', 'is_work', )
    list_filter = ('is_work', )

    fieldsets = (
        (None, {'fields': ('cellphone', 'password')}),
        (u'Персональные данные', {'fields': ('name', 'is_work', )}),
        (u'Права доступа', {'fields': ('is_staff', 'is_active', )}),
    )

    add_fieldsets = (
        (None, {
            'classes': ('wide', ),
            'fields': ('cellphone', 'name', 'password1', 'password2')
        }),
    )

    search_fields = ('cellphone', )
    ordering = ('cellphone', )
    filter_horizontal = ()

admin.site.register(Courier, CourierAdmin)

########################################################################################################################
