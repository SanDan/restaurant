# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser
)
import re

########################################################################################################################

APPLICATION_NAME = 'Персонал'
DATABASE_NAME = 'rest_personal'

def get_raw_name(raw):
    return '{0}_{1}'.format(DATABASE_NAME, raw)

class StringWithTitle(str):
    def __new__(cls, value, title):
        instance = str.__new__(cls, value)
        instance._title = title
        return instance

    def title(self):
        return self._title

    __copy__ = lambda  self: self
    __deepcopy__ = lambda self, memodict: self

get_class = lambda x: globals()[x]

########################################################################################################################

class RestUserManager(BaseUserManager):
    def create_user(self, email, name, password=None):
        if not email:
            raise ValueError(u'Пользователь должен иметь email')
        user = self.model(
            email=self.normalize_email(email),
            name=name,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, name, password):

        user = self.create_user(email=email,
                                password=password,
                                name=name
                                )
        user.is_admin = True
        user.save(using=self._db)
        return user


class RestUser(AbstractBaseUser):
    email = models.EmailField(
        verbose_name=u'email',
        max_length=255,
        unique=True,
    )
    name = models.CharField(max_length=200, verbose_name=u'имя')
    is_active = models.BooleanField(default=True, verbose_name=u'активность')
    is_admin = models.BooleanField(default=False, verbose_name=u'администратор')

    objects = RestUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    def __unicode__(self):
        return 'Manager - ' + self.name

    class Meta:
        verbose_name = u'менеджер'
        verbose_name_plural = u'менеджеры'

        app_label = StringWithTitle(DATABASE_NAME, APPLICATION_NAME)
        db_table = get_raw_name('rest_user')

    def get_full_name(self):
        return self.email

    def get_short_name(self):
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

    @property
    def is_staff(self):
        return self.is_admin

########################################################################################################################

class CourierManager(BaseUserManager):

    def is_cellphone(self, cellphone):
        pattern = '[0-9]'
        if re.match(pattern, cellphone):
            return cellphone
        else:
            raise ValueError(u'Номер телефона должен состоять только из цифр.')

    def create_user(self, cellphone, name, password=None):
        if not cellphone:
            raise ValueError(u'Пользователь должен иметь номер телефона.')
        user = self.model(
            cellphone=self.is_cellphone(cellphone),
            name=name,
            is_work=False,
        )
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, cellphone, name, password):

        user = self.create_user(cellphone=cellphone,
                                password=password,
                                name=name
                                )
        user.is_staff = False
        user.save(using=self._db)
        return user


class Courier(AbstractBaseUser):
    cellphone = models.CharField(
        verbose_name=u'телефон',
        max_length=7,
        unique=True,
    )
    name = models.CharField(max_length=200, verbose_name=u'имя')
    is_work = models.BooleanField(default=False, verbose_name=u'работает')
    is_active = models.BooleanField(default=True, verbose_name=u'активный')
    is_staff = models.BooleanField(default=False, verbose_name=u'сотрудник')

    def __unicode__(self):
        return 'Courier - ' + self.name

    class Meta:
        verbose_name = u'курьер'
        verbose_name_plural = u'курьеры'

        app_label = StringWithTitle(DATABASE_NAME, APPLICATION_NAME)
        db_table = get_raw_name('courier')

    objects = CourierManager()

    USERNAME_FIELD = 'cellphone'
    REQUIRED_FIELDS = ['name']

    def get_full_name(self):
        return u'Курьер - ' + self.name

    def get_short_name(self):
        return u'Курьер - ' + self.name

    def __unicode__(self):
        return u'Курьер - ' + self.name

    def has_perm(self, perm, obj=None):
        return True

    def has_module_perms(self, app_label):
        return True

########################################################################################################################