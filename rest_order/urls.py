# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from .views import OrdersForCourier

urlpatterns = patterns('',
    url(r'order-for-courier$', OrdersForCourier.as_view(), name='post_order_for_courier'),
)
