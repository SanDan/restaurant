# -*- coding: utf-8 -*-
from django.contrib import admin
import csv
from django.http import HttpResponse

from .models import Order
from rest_personal.models import Courier

########################################################################################################################

def export_as_csv_action(description="Export selected objects as CSV file",
                         fields=None, exclude=None, header=True):
    """
    This function returns an export csv action
    'fields' and 'exclude' work like in django ModelForm
    'header' is whether or not to output the column names as the first row
    """
    def export_as_csv(modeladmin, request, queryset):
        """
        Generic csv export admin action.
        based on http://djangosnippets.org/snippets/1697/
        """
        opts = modeladmin.model._meta
        field_names = set([field.name for field in opts.fields])
        if fields:
            fieldset = set(fields)
            field_names = field_names & fieldset
        elif exclude:
            excludeset = set(exclude)
            field_names = field_names - excludeset

        response = HttpResponse(content_type='text/csv; charset=utf-8')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % unicode(opts).replace('.', '_')

        writer = csv.writer(response)
        if header:
            headers_dict = [u'Время принятия заказа', u'Статус', u'Курьер', u'Адрес']
            writer.writerow([unicode(head).encode('utf-8') for head in headers_dict])
        for obj in queryset:
            writer.writerow([unicode(getattr(obj, field)).encode('utf-8') for field in field_names])
        return response
    export_as_csv.short_description = description
    return export_as_csv

########################################################################################################################

class OrderAdmin(admin.ModelAdmin):

    list_display = ('date_create', 'address', 'status', 'courier', )
    list_filter = ('status', )
    readonly_fields = ('date_create', )
    actions = [export_as_csv_action(u'Экспортировать', fields=['date_create', 'address', 'status', 'courier'], header=True)]

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if db_field.name == "courier":
            kwargs["queryset"] = Courier.objects.filter(is_work=True)
        return super(OrderAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

admin.site.register(Order, OrderAdmin)

########################################################################################################################