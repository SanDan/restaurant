# -*- coding: utf-8 -*-
from django.db import models
from import_export import resources
from rest_personal.models import Courier

########################################################################################################################
###Сниппет для измения названия приложения в админке
APPLICATION_NAME = 'Заказы'
DATABASE_NAME = 'rest_order'

def get_raw_name(raw):
    return '{0}_{1}'.format(DATABASE_NAME, raw)

class StringWithTitle(str):
    def __new__(cls, value, title):
        instance = str.__new__(cls, value)
        instance._title = title
        return instance

    def title(self):
        return self._title

    __copy__ = lambda  self: self
    __deepcopy__ = lambda self, memodict: self

get_class = lambda x: globals()[x]

########################################################################################################################

class Order(models.Model):
    ORDER_STATUS = (
        (u'Новый', u'Новый'),
        (u'Готов', u'Готов'),
        (u'В пути', u'В пути'),
        (u'Доставлен', u'Доставлен'),
    )

    date_create = models.DateTimeField(auto_now_add=True, verbose_name=u'время принятия')
    address = models.CharField(max_length=400, verbose_name=u'адрес')
    status = models.CharField(max_length=50, choices=ORDER_STATUS, verbose_name=u'статус')
    courier = models.ForeignKey(Courier, blank=True, null=True, verbose_name=u'курьер')

    def couriers_working(self):
        return Courier.objects.filter(is_work=True)

    couriers_working.short_description = "Couriers"

    couriers = property(couriers_working)
    def __unicode__(self):
        return 'Order - ' + self.address

    class Meta:
        verbose_name = u'Заказ'
        verbose_name_plural = u'Заказы'

        app_label = StringWithTitle(DATABASE_NAME, APPLICATION_NAME)
        db_table = get_raw_name('order')

########################################################################################################################
